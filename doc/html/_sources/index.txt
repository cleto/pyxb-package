.. PyXB documentation master file

.. Sphinx standard indentations
   # with overline, for parts
   * with overline, for chapters
   =, for sections
   -, for subsections
   ^, for subsubsections
   ", for paragraphs

.. role:: namespace(literal)

################################
PyXB: Python XML Schema Bindings
################################

PyXB ("pixbee") is a pure `Python <http://www.python.org>`_ package that
generates Python source code for classes that correspond to data structures
defined by `XMLSchema <http://www.w3.org/XML/Schema>`_.  The generated classes
support bi-directional conversion between `XML <http://www.w3.org/XML/>`_
documents and Python objects.  In concept it is similar to `JAXB
<http://en.wikipedia.org/wiki/JAXB>`_ for Java and `CodeSynthesis XSD
<http://www.codesynthesis.com/products/xsd/>`_ for C++.  A
:ref:`thirty_sec_example` is at the bottom of this page.  Step-by-step
examples are in :ref:`userReference`.

************
Getting Help
************

PyXB is distributed on `SourceForge <http://sourceforge.net/projects/pyxb>`_.

For support, consult the `Help Forum
<https://sourceforge.net/projects/pyxb/forums/forum/956708>`_, or subscribe to
and email the `mailing list
<https://sourceforge.net/mailarchive/forum.php?forum_name=pyxb-users>`_.

To file a bug report or see the status of defects reported against the current
release, visit the `Trac database <http://sourceforge.net/apps/trac/pyxb/>`_.

For a history of releases, see :ref:`releases`.

********
Contents
********
.. toctree::
   :maxdepth: 2

   overview
   examples
   releases
   architecture
   userref_index
   maintref

.. _thirty_sec_example:

*********************
Thirty Second Example
*********************

An example of a program using PyXB to interact with a `web service
<http://wiki.cdyne.com/wiki/index.php?title=CDYNE_Weather>`_ using an
automatically-generated module.  First, retrieve the WSDL and generate the
bindings::

 llc[766]$ pyxbgen \
       --wsdl-location=http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL \
       --module=weather \
       --write-for-customization
 Retrieving WSDL from http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL
 WARNING:pyxb.binding.basis:Unable to convert DOM node {http://www.w3.org/2001/XMLSchema}schema at Weather.asmx?WSDL[4:4] to binding
 PS urn:uuid:ea640f34-4869-11e2-924d-c8600024e903
 WARNING:pyxb.binding.generate:Complex type {http://ws.cdyne.com/WeatherWS/}ArrayOfWeatherDescription renamed to ArrayOfWeatherDescription_
 WARNING:pyxb.binding.generate:Complex type {http://ws.cdyne.com/WeatherWS/}ForecastReturn renamed to ForecastReturn_
 WARNING:pyxb.binding.generate:Complex type {http://ws.cdyne.com/WeatherWS/}WeatherReturn renamed to WeatherReturn_
 Python for http://ws.cdyne.com/WeatherWS/ requires 1 modules

Then write a program that uses them:

.. literalinclude:: ../examples/weather/client_get.py

And run it::

 llc[767]$ python client_get.py
 Weather forecast for Saint Paul, MN:
  Monday, December 17 2012: Partly Cloudy, from 15 to 26
  Tuesday, December 18 2012: , from 19 to 32
  Wednesday, December 19 2012: Partly Cloudy, from 18 to 32
  Thursday, December 20 2012: Mostly Cloudy, from 16 to 22
  Friday, December 21 2012: Sunny, from 8 to 20
  Saturday, December 22 2012: Partly Cloudy, from 8 to 21
  Sunday, December 23 2012: Partly Cloudy, from 3 to 19

That's it.

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`search`

.. ignored
   ## Local Variables:
   ## fill-column:78
   ## indent-tabs-mode:nil
   ## End:
