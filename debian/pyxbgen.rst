=========
 pyxgen
=========

---------------------------------------------
generate bindings from a set of XML schemas
---------------------------------------------

:Date: 2013-08-09
:Version: 0.1
:Manual section: 1
:Manual group: General Commands Manual

SYNOPSIS
========

  pyxgen [options] [more schema locations...

DESCRIPTION
===========

pyxgen generates bindings from a set of XML schemas. It is used to
translate XML schema and WSDL documents into Python data bindings.
There are two key components to generating bindings:

- The URI to the definition of the data structures.  This can be a
  URL, or a path to a local file.

- The module path into which the binding will be installed.  This is
  the path that users of the binding will import.


OPTIONS
=======

--version             show program's version number and exit
-h, --help            show this help message and exit


* Identifying Schema: specify and locate schema for which bindings should be generated.

  -u FILE_or_URL, --schema-location=FILE_or_URL  Add the location of an entrypoint schema. The provided value should be a URL; if it does not have a URL scheme (e.g., C{http:}), it is assumed to be a file, and if it is not an absolute path is located relative to the C{schemaRoot}.

  --schema-root=DIRECTORY  The directory from which entrypoint schemas specified as relative file paths will be read.

  --schema-stripped-prefix=TEXT  Optional string that is stripped from the beginning of schemaLocation values before loading from them. This applies only to the values of schemaLocation attributes in C{import} and C{include} elements.  Its purpose is to convert absolute schema locations into relative ones to allow offline processing when all schema are available in a local directory. See C{schemaRoot}.

  --location-prefix-rewrite=TEXT  Add a rewrite entry for schema locations. Parameter values are strings of the form C{pfx=sub}. The effect is that a schema location that begins with C{pfx} is rewritten so that it instead begins with C{sub}.

  --uri-content-archive-directory=DIRECTORY  The directory path into which any content retrieved by URI will be written. This serves as a local cache, and to give you an opportunity to inspect material retrieved from some other system. @rtype: C{str}


* Configuring Bindings: specify where generated bindings should be written, and how they will be accessed from Python.

  -m MODULE, --module=MODULE  Add a module name corresponding to an entrypoint schema. The namespace defined by the corresponding schema will be written to a binding using the given module name, adjusted by L{modulePrefix}.

  --module-prefix=MODULE  The prefix for binding modules. The base name for the module holding a binding is taken from the moduleList, moduleMap, or an XMLNS prefix associated with the namespace in a containing schema. This value, if present, is used as a prefix to allow a deeper module hierarchy.

  --binding-root=DIRECTORY  The directory path into which generated bindings will be written. @rtype: C{str} -r, --write-for-customization Indicates whether the binding Python code should be written into a sub-module for customization. If enabled, a module C{path.to.namespace} will be written to the file C{path/to/raw/namespace.py}, so that the file C{path/to/namespace.py} can import it and override behavior. This option turns on the feature.

  --no-write-for-customization  Indicates whether the binding Python code should be written into a sub-module for customization. If enabled, a module C{path.to.namespace} will be written to the file C{path/to/raw/namespace.py}, so that the file C{path/to/namespace.py} can import it and override behavior. This option turns off the feature (I{default}).

* Reading Namespace Archives: locating and loading (or inhibiting load of) namespace archives.

  --archive-path=PATH  A colon-separated list of paths from which namespace archives can be read. The default path is the contents of the C{PYXB_ARCHIVE_PATH} environment variable, or the standard path configured at installation time. Any file with the extension C{.wxs} found in one of these directories is examined to see whether it is a namespace archive.

  --pre-load-archive=FILE  Name of a file containing a stored archive from which namespaces should be read prior to processing schema. Files to be pre-loaded are not affected by C{noLoadNamespace}.


  --no-load-namespace=URI  Mark that the specified namespace should not be loaded from an archive. Use this when you are generating bindings for an application that has a restricted profile of a namespace that would otherwise be read from an archive.  Be aware that this removes any knowledge of any archive in which this namespace is present as a non-private member.

*  Writing Namespace Archives: control the location and content of a namespace archive corresponding to a binding generation.

   --archive-to-file=FILE  Optional file into which the archive of namespaces will be written. Subsequent generation actions can read pre-parsed namespaces from this file, and therefore reference the bindings that were built earlier rather than re-generating them. The file name should normally end with C{.wxs}.

   --public-namespace=URI  Indicates, for specific namespaces, whether their visibility in the archive should be public or private. This option adds the namespace as a public archive member.

   --private-namespace=URI  Indicates, for specific namespaces, whether their visibility in the archive should be public or private. This option adds the namespace as a private archive member.


   --default-namespace-public  Indicates whether unmentioned namespaces will be public or private (default) in the archive. A namespace is I{mentioned} if it is the target namespace of an entrypoint schema, or appears in a namespace visibility specification.  I.e., this default applies only to namespaces that are modified as a result of including some schema, which is generally a local customization of something. This option makes the default C{public} (I{default}).

   --default-namespace-private  Indicates whether unmentioned namespaces will be public or private (default) in the archive. A namespace is I{mentioned} if it is the target namespace of an entrypoint schema, or appears in a namespace visibility specification.  I.e., this default applies only to namespaces that are modified as a result of including some schema, which is generally a local customization of something. This option makes the default C{private}.

*  Configuring Binding Code Generation: control the style and content of the generated bindings.  This is not well-supported, and you are advised to pretend these options don't exist.

   --validate-changes  Indicates whether the bindings should validate mutations against the content model. This option turns on validation (default).

   --no-validate-changes  Indicates whether the bindings should validate mutations against the content model. This option turns off validation.

* Miscellaneous Options: anything else.

  --logging-config-file=FILE  A file provided to L{logging.config.fileConfig} to control log messages. In the absence of other configuration the Python standard logging infrastructure is used in its default configuration. @rtype: C{str}

* Maintainer Options: Don't use these. They don't exist. If they did, they'd do different things at different times, and if you used them you'd probably be sorry.

  --allow-absent-module  Indicates whether the code generator is permitted to process namespace for which no module path can be determined. Use this only when generating bindings that will not be referenced by other bindings. This option turns on the feature.

  --no-allow-absent-module  Indicates whether the code generator is permitted to process namespace for which no module path can be determined. Use this only when generating bindings that will not be referenced by other bindings. This option turns off the feature (default).

  --allow-builtin-generation  Indicates whether bindings will be written for namespaces that are built-in to PyXB. This must be enabled when building bindings for the XML, XMLSchema instance, and other built-in namespaces.  Normally generation of these namespaces is inhibited lest it produce inconsistencies. This option turns on the feature.

  --no-allow-builtin-generation  Indicates whether bindings will be written for namespaces that are built-in to PyXB. This must be enabled when building bindings for the XML, XMLSchema instance, and other built-in namespaces.  Normally generation of these namespaces is inhibited lest it produce inconsistencies. This option turns off the feature (default).

* WSDL Options: options relevant to generating bindings from WSDL service descriptions

  -W FILE_or_URL, --wsdl-location=FILE_or_URL  Generate bindings for the C{types} element of the WSDL at this location.


COPYRIGHT
=========

This manual is extracted from pyxbgen help messages and PyXB
documentation (C) 2009-2012 Peter A. Bigot
<pabigot@users.sourceforge.net>. Written by Cleto Martín
<cleto@debian.org>.


SEE ALSO
========

* `docutils <http://docutils.sourceforge.net>`__
* `linux man page howto <http://tldp.org/HOWTO/Man-Page/>`__
